from data_processing import DataProcessing
from RN import RN50
import cv2 
import matplotlib.pyplot as plt
from keras.layers import Input
from keras.models import Model, load_model



X_train_orig, Y_train_orig, X_test_orig, Y_test_orig, classes = DataProcessing.load_dataset()

# Normalize data
X_train = X_train_orig/255
X_test = X_test_orig/255

#Convert training and test lables to one hot matrics
Y_train = DataProcessing.convert_to_one_hot(Y_train_orig,6).T
Y_test = DataProcessing.convert_to_one_hot(Y_test_orig,6).T



X_input= Input((64,64,3))
# ResNET50 defines structure of Resnet
model__ = RN50(input_shape=(64,64,3), classes=6)
model = RN50.resnet(model__)
#Initiallize model
#model = Model(inputs = X_input, outputs = output, name = 'ResNet50' )  
#compile model
model.compile(optimizer='adam', loss= 'categorical_crossentropy', metrics= ['accuracy'])
#train modeö on rain dataset
model.fit(X_train, Y_train, epochs = 3, batch_size = 32)
# evaluate model on test data
preds = model.evaluate(X_test, Y_test)
print ("Loss = " + str(preds[0]))
print ("Test Accuracy = " + str(preds[1]))

# evaluate model on image
img_pth = 'my_image.jpg'
img= image.load_img(img_pth, target_size=(64,64))
x= image.img_to_array(img)
print(x.shape)
x = np.expand_dims(x, axis=0)
print(x.shape)
my_img= cv2.imread(img_pth)
plt.imshow(my_img)
print(model.predict(x))
