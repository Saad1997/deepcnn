import numpy as np
from keras import layers
from keras.layers import Input, Add, Dense, Activation, ZeroPadding2D,BatchNormalization, MaxPooling2D, AveragePooling2D, Flatten
from keras.layers.convolutional import Conv2D
from keras.models import Model, load_model
from keras.preprocessing import image 
from keras.utils import layer_utils
from keras. utils.data_utils import get_file
from keras.applications.imagenet_utils import preprocess_input
import pydot
from IPython.display import SVG
from keras.utils.vis_utils import model_to_dot
from keras.utils.vis_utils import plot_model
from keras.applications.resnet import ResNet50 
from keras.initializers import glorot_uniform
from matplotlib.pyplot import imshow
from random import seed
import cv2 
import matplotlib.pyplot as plt
# %matplotlib inline
import keras.backend as K
K.set_image_data_format('channels_last')
K.set_learning_phase(1)


class RN50:
    def __init__(self, input_shape, classes):
        self.input_shape=input_shape
        self.classes=classes
        X_input= Input(input_shape) #define input tensor of shape input
        X = ZeroPadding2D((3,3))(X_input)

        #stage1
        
        X= Conv2D(64, (7,7), strides=(2,2), name= 'conv1', kernel_initializer=glorot_uniform(seed(0)))(X)
        X= BatchNormalization(axis=3, name= 'bn_conv1')(X)
        X = Activation('relu')(X)
        X = MaxPooling2D((3,3),strides = (2,2))(X)
        print('2nd pass : {}'. format(X.shape))
        X =self.convolutional_block(X, f=3, filters=[64,64,256], stage=2, block='a', s=1)
        print('2nd pass : {}'. format(X.shape))
        
        X = self.identity_block(X, 3, [64,64,256], stage=2, block = 'b')
        X = self.identity_block(X,3, [64,64,256], stage=2, block= 'c' )
        
        
        
        X= self.convolutional_block(X, f=3, filters=[28,128,512], stage =3, block = 'a', s=2)
        X= self.identity_block(X, 3, [128,128,512], stage=3, block = 'b')
        X= self.identity_block(X,3,[128,128,512], stage=3, block='c')
        X= self.identity_block(X,3,[128,128,512],stage=3, block = 'd')
        print('3rd pass')
        
        # stage 4
        X = self.convolutional_block(X, f = 3, filters = [256, 256, 1024], stage = 4, block='a', s = 2)
        X = self.identity_block(X, 3, [256, 256, 1024], stage=4, block='b')
        X = self.identity_block(X, 3, [256, 256, 1024], stage=4, block='c')
        X = self.identity_block(X, 3, [256, 256, 1024], stage=4, block='d')
        X = self.identity_block(X, 3, [256, 256, 1024], stage=4, block='e')
        X = self.identity_block(X, 3, [256, 256, 1024], stage=4, block='f')
        print('4th pass')
        
        #stage 5
        X= self.convolutional_block(X,f=3, filters = [256,256,2048], stage = 5, block ='a', s=2)
        X = self.identity_block(X, 3, [512, 512, 2048], stage=5, block='b')
        X= self.identity_block(X,3,[512,512,2048], stage=5, block='c')
        print('5th pass')
        
        X= AveragePooling2D((2,2), name = "avg_pool")(X)
        X= Flatten()(X)
        output= Dense(6, activation='softmax', name= 'fc' +str(classes), kernel_initializer= glorot_uniform(seed(0)))(X)
        self.model = Model(inputs = X_input, outputs = output, name = 'ResNet50' )
    def resnet(self):
        
        return self.model
    

        
        
        
    def identity_block(self,X,f,filters, stage , block):
        ##implementation of identity function # its a block where input has same dimension to output so we don't need additional Conv2D layers in shortcut path

        """
        X : input tensor
        f: integer specifyung shape of middle conv window
        filters: python list of integers, defining number of filters
        stage: integer, used to name layers , dependiong on their position 
        block: string character, used to name layers 
        Returns: X: output of identity block, tensor of shape(n_H, n_W, n_C)
        """
        # defining name basis
        conv_name_base = 'res' + str(stage) + block + '_branch'
        bn_name_base = 'bn' + str(stage) + block + '_branch'
        
        # Retrieve Filters
        F1, F2, F3 = filters
        
        # Save the input value. You'll need this later to add back to the main path. 
        X_shortcut = X
        
        # First component of main path
        X = Conv2D(filters = F1, kernel_size = (1, 1), strides = (1,1), padding = 'valid', name = conv_name_base + '2a', kernel_initializer = glorot_uniform(seed=0))(X)
        X = BatchNormalization(axis = 3, name = bn_name_base + '2a')(X)
        X = Activation('relu')(X)

        
        # Second component of main path (≈3 lines)
        X = Conv2D(filters = F2, kernel_size = (f, f), strides = (1,1), padding = 'same', name = conv_name_base + '2b', kernel_initializer = glorot_uniform(seed=0))(X)
        X = BatchNormalization(axis = 3, name = bn_name_base + '2b')(X)
        X = Activation('relu')(X)

        # Third component of main path (≈2 lines)
        X = Conv2D(filters = F3, kernel_size = (1, 1), strides = (1,1), padding = 'valid', name = conv_name_base + '2c', kernel_initializer = glorot_uniform(seed=0))(X)
        X = BatchNormalization(axis = 3, name = bn_name_base + '2c')(X)

        # Final step: Add shortcut value to main path, and pass it through a RELU activation (≈2 lines)
        X = Add()([X, X_shortcut])
        X = Activation('relu')(X)
        
        
        return X
    
    def convolutional_block(self,X, f, filters, stage, block, s ):
        """
        Implementation of the convolutional block as defined in Figure 4
        
        Arguments:
        X -- input tensor of shape (m, n_H_prev, n_W_prev, n_C_prev)
        f -- integer, specifying the shape of the middle CONV's window for the main path
        filters -- python list of integers, defining the number of filters in the CONV layers of the main path
        stage -- integer, used to name the layers, depending on their position in the network
        block -- string/character, used to name the layers, depending on their position in the network
        s -- Integer, specifying the stride to be used
        
        Returns:
        X -- output of the convolutional block, tensor of shape (n_H, n_W, n_C)
        """
        
        # defining name basis
        conv_name_base = 'res' + str(stage) + block + '_branch'
        bn_name_base = 'bn' + str(stage) + block + '_branch'
        
        # Retrieve Filters
        F1, F2, F3 = filters
        
        # Save the input value
        X_shortcut = X


        ##### MAIN PATH #####
        # First component of main path 
        X = Conv2D(F1, (1, 1), strides = (s,s), name = conv_name_base + '2a', kernel_initializer = glorot_uniform(seed=0))(X)
        X = BatchNormalization(axis = 3, name = bn_name_base + '2a')(X)
        X = Activation('relu')(X)

        # Second component of main path (≈3 lines)
        X = Conv2D(filters = F2, kernel_size = (f, f), strides = (1,1), padding = 'same', name = conv_name_base + '2b', kernel_initializer = glorot_uniform(seed=0))(X)
        X = BatchNormalization(axis = 3, name = bn_name_base + '2b')(X)
        X = Activation('relu')(X)


        # Third component of main path (≈2 lines)
        X = Conv2D(filters = F3, kernel_size = (1, 1), strides = (1,1), padding = 'valid', name = conv_name_base + '2c', kernel_initializer = glorot_uniform(seed=0))(X)
        X = BatchNormalization(axis = 3, name = bn_name_base + '2c')(X)


        ##### SHORTCUT PATH #### (≈2 lines)
        X_shortcut = Conv2D(filters = F3, kernel_size = (1, 1), strides = (s,s), padding = 'valid', name = conv_name_base + '1',
                            kernel_initializer = glorot_uniform(seed=0))(X_shortcut)
        X_shortcut = BatchNormalization(axis = 3, name = bn_name_base + '1')(X_shortcut)

        # Final step: Add shortcut value to main path, and pass it through a RELU activation (≈2 lines)
        X = Add()([X, X_shortcut])
        X = Activation('relu')(X)
        
        
        return X
