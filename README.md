# Deep Neural Networks

This repository is part of my deep learning self learning and is going to contain multiple deep learning networks.
Useage of each network is documented here.

## ResNet50

This networkfolder has 2 classes:

1- **RN50:** which carries structure of ResNet50 model alogn with identiny and convolutional blocks which provides residual connection in ResNet.

2- **DataProcessing:** this  class is used to access train and testing data and to create mini batches. So this is a data processing pipeline that we require before training our model on specific data, here i am using sign language dataset.

Finally train_and_test file carries code to train and test the ResNet model. Also an image is given to test the network accuracy. To use network , clone this repo and run "python3 train_and_test.py". Soon a requirment.txt will be added containing all the depenencies required to used self coded ResNet50.
